//
//  ViewController.m
//  CoutonNotificationDemo
//
//  Created by Sokna Ly on 11/26/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import "ViewController.h"
#import "SNCroutonNotificationView.h"
@interface ViewController ()

@end

@implementation ViewController{
  SNCroutonNotificationView *messageView ;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  messageView = [[SNCroutonNotificationView alloc] initWithNavigationBar:self.navigationController.navigationBar
                                                                 message:@"Data has been downloaded!"
                                                                duration:SNCoutonNotificationDurationMedium];
  messageView.backgroundColor = [UIColor yellowColor];
  messageView.textAlignment = NSTextAlignmentCenter;
  messageView.fontSize = 12;

}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}
- (IBAction)showButton:(id)sender {
  [messageView show];
}

@end
