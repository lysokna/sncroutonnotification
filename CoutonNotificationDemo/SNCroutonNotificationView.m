//
//  SNCoutonNotificationView.m
//  CoutonNotificationDemo
//
//  Created by Sokna Ly on 11/26/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import "SNCroutonNotificationView.h"
#define SNCroutonRect CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,40)
@interface SNCroutonNotificationView()

@property (nonatomic, strong) UIView *rootView;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *editedName;


@end

@implementation SNCroutonNotificationView

#pragma mark - Initalize

- (instancetype)init {
  return [self initWithFrame:SNCroutonRect];
}

- (instancetype)initWithRootView:(UIView *)view {
  
  return [self initWithRootView:view message:nil duration:SNCoutonNotificationDurationMedium];
}

- (instancetype)initWithRootView:(UIView *)view
                         message:(NSString *)message {
  
  return [self initWithRootView:view message:message duration:SNCoutonNotificationDurationMedium];
}

- (instancetype)initWithRootView:(UIView *)view
                         message:(NSString *)message
                        duration:(SNCroutonNotificationDuration)duration {
  return [self initWithFrame:SNCroutonRect rootView:view message:message duration:duration];
}

- (instancetype)initWithFrame:(CGRect)frame
                navigationBar:(UINavigationBar *)navigationBar
                      message:(NSString *)message
                     duration:(SNCroutonNotificationDuration)duration {
  
  return [self initWithFrame:frame rootView:navigationBar message:message duration:duration];
  
}

- (instancetype)initWithNavigationBar:(UINavigationBar *)navigationBar
                              message:(NSString *)message
                             duration:(SNCroutonNotificationDuration)duration {
  
  return [self initWithFrame:SNCroutonRect navigationBar:navigationBar message:message duration:duration];
  
}

//Designated initializer

- (instancetype)initWithFrame:(CGRect)frame
                     rootView:(UIView *)view
                      message:(NSString *)message
                     duration:(SNCroutonNotificationDuration)duration {
  if (self = [super initWithFrame:frame]) {
    [self setupView];
    _rootView = view;
    _message = message;
    _duration = duration;
  }
  return self;
}


- (void)setupView {
  _textColor = [UIColor blackColor];
  _fontSize = [UIFont systemFontSize];
  _font = [UIFont systemFontOfSize:_fontSize];
  _duration = SNCoutonNotificationDurationMedium;
  _textAlignment = NSTextAlignmentLeft;
  [self setBackgroundColor:[UIColor whiteColor]];
  [self.textLabel setTextColor:[UIColor blackColor]];
  [self.textLabel setText:_message];
  [self.textLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
  [self addSubview:self.textLabel];
  [self addShadow];
}

- (void)setupAutoLayoutForLabel {
  NSMutableArray *constraints = [NSMutableArray array];
  NSDictionary *views = @{@"super":self,
                          @"textLabel":self.textLabel};
  
  NSArray *verticalLabelConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[textLabel]-|"
                                                                              options:kNilOptions
                                                                              metrics:nil
                                                                                views:views];
  NSArray *horizontalLabelConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textLabel]-|"
                                                                                options:kNilOptions
                                                                                metrics:nil
                                                                                  views:views];
  [constraints addObjectsFromArray:verticalLabelConstraints];
  [constraints addObjectsFromArray:horizontalLabelConstraints];
  [NSLayoutConstraint activateConstraints:constraints];
}

#pragma mark - Lazy Properties

- (UILabel *)textLabel {
  if (!_textLabel) {
    _textLabel = [[UILabel alloc] init];
  }
  return _textLabel;
}


#pragma mark - Accessor

- (void)setTextColor:(UIColor *)textColor {
  [self.textLabel setTextColor:textColor];
}

- (void)setMessage:(NSString *)message {
  [self.textLabel setText:message];
  [self setupAutoLayoutForLabel];
}

- (void)setFont:(UIFont *)font {
  [self.textLabel setFont:font];
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment {
  [self.textLabel setTextAlignment:textAlignment];
}

- (NSTimeInterval)animationDuration {
  NSTimeInterval duration;
  switch (self.duration) {
    case SNCoutonNotificationDurationShort:
      duration = 2;
      break;
    case SNCoutonNotificationDurationMedium:
      duration = 4;
      break;
    case SNCoutonNotificationDurationLong:
      duration = 8;
    default:
      duration = self.duration;
      break;
  }
  return duration;
}

#pragma mark - Public Methods

- (void)show {
  NSAssert(self.rootView != nil, @"Root view can't be nil!");
  CGRect frame = self.rootView.frame;
  if ([self.rootView isKindOfClass:[UINavigationBar class]]) {
    frame.origin.y = CGRectGetMaxY(self.rootView.frame);
    [self setFrame:CGRectMake(0, fabs(self.frame.size.height-frame.origin.y), frame.size.width, frame.size.height)];
    [self.rootView.superview insertSubview:self belowSubview:self.rootView];
  }
  if ([self.delegate respondsToSelector:@selector(croutonNotificationViewWillAppear:)]) {
    [self.delegate croutonNotificationViewWillAppear:self];
  }
  [UIView animateWithDuration:0.5
                        delay:0.0
                      options:UIViewAnimationOptionCurveEaseInOut animations:^{
                        self.frame = frame;
                      } completion:^(BOOL finished) {
                        if ([self.delegate respondsToSelector:@selector(croutonNotificationViewDidAppear:)]) {
                          [self.delegate croutonNotificationViewDidAppear:self];
                        }
                        [self performSelector:@selector(dismiss)];
                      }];
}

- (void)dismiss {
  if ([self.delegate respondsToSelector:@selector(croutonNotificationViewWillDisappear:)]) {
    [self.delegate croutonNotificationViewWillDisappear:self];
  }
  [UIView animateWithDuration:0.5
                        delay:[self animationDuration]
                      options:UIViewAnimationOptionCurveEaseOut
                   animations:^{
                     CGRect frame = self.rootView.frame;
                     frame.origin.y = 24;
                     self.frame = frame;
                   } completion:^(BOOL finished) {
                     if ([self.delegate respondsToSelector:@selector(croutonNotificationViewDidDisappear:)]) {
                       [self.delegate croutonNotificationViewDidDisappear:self];
                     }
                     [self removeFromSuperview];
                   }];
}

- (void)addShadow {
  CALayer *layer = self.layer;
  [layer setShadowColor:[UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor];
  [layer setShadowRadius:1.5f];
  [layer setShadowOffset:CGSizeMake(0, 0)];
  [layer setShadowOpacity:0.9f];
  [layer setMasksToBounds:NO];
  UIEdgeInsets shadowInsets = UIEdgeInsetsMake(-2, -2, -2.0f, -2);
  UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(self.bounds, shadowInsets)];
  [layer setShadowPath:shadowPath.CGPath];
}




@end
