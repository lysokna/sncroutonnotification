//
//  AppDelegate.h
//  CoutonNotificationDemo
//
//  Created by Sokna Ly on 11/26/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

