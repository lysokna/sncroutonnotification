//
//  SNCoutonNotificationView.h
//  CoutonNotificationDemo
//
//  Created by Sokna Ly on 11/26/15.
//  Copyright © 2015 Sokna Ly. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SNCroutonNotificationDuration){
  SNCoutonNotificationDurationShort = 0,
  SNCoutonNotificationDurationMedium,
  SNCoutonNotificationDurationLong
};

@protocol SNCroutonNotificationDelegate;

@interface SNCroutonNotificationView : UIView

@property (nonatomic, strong) UIColor *textColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIFont *font UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) NSString *message;
@property (nonatomic) NSTextAlignment textAlignment UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat fontSize UI_APPEARANCE_SELECTOR;
@property (nonatomic) SNCroutonNotificationDuration duration;
@property (nonatomic, weak) id<SNCroutonNotificationDelegate> delegate;

- (instancetype)initWithNavigationBar:(UINavigationBar *)navigationBar
                              message:(NSString *)message
                             duration:(SNCroutonNotificationDuration)duration;

- (instancetype)initWithFrame:(CGRect)frame
                     rootView:(UIView *)view
                      message:(NSString *)message
                     duration:(SNCroutonNotificationDuration)duration;

- (instancetype)initWithFrame:(CGRect)frame
                navigationBar:(UINavigationBar *)navigationBar
                      message:(NSString *)message
                     duration:(SNCroutonNotificationDuration)duration;

- (void)show;

@end

@protocol SNCroutonNotificationDelegate <NSObject>

@optional

- (void)croutonNotificationViewWillAppear:(SNCroutonNotificationView *)crountonView;
- (void)croutonNotificationViewDidAppear:(SNCroutonNotificationView *)crountonView;
- (void)croutonNotificationViewWillDisappear:(SNCroutonNotificationView *)crountonView;
- (void)croutonNotificationViewDidDisappear:(SNCroutonNotificationView *)crountonView;

@end
